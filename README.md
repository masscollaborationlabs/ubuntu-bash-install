# Calckey Ubuntu install shell script

This is forked from <https://github.com/joinmisskey/bash-install> for use with Calckey.

Install Calckey with one shell script!  

You can install calckey on an Ubuntu server just by answering some questions.  

There is also an update script.

## Requirements

1. A domain
2. An Ubuntu Server
3. Git
4. Docker (optional)

## Configure Cloudflare

If you are using Nginx and Cloudflare, you must configure Cloudflare:

- Set DNS.
- On SSL/TLS setting tab, switch the encryption mode to "Full".
- Turn off code minification.

## Steps

### 1. Prepare

Make sure all packages are up to date and reboot.

```sh
sudo apt update; sudo apt full-upgrade -y; sudo reboot
```

### 2. Start the installation

Reconnect SSH and let's start installing Calckey.

```sh
wget https://codeberg.org/calckey/ubuntu-bash-install/raw/branch/main/ubuntu.sh -O ubuntu.sh; sudo bash ubuntu.sh
```

### 3. Updating

There is also an update script.

First, download the script.

```sh
wget https://codeberg.org/calckey/ubuntu-bash-install/raw/branch/main/update.ubuntu.sh -O update.sh
```

Run it when you want to update Calckey.

```sh
sudo bash update.sh
```

- In the systemd environment, the `-r` option can be used to update and reboot the system.
- In the docker environment, you can specify repository:tag as an argument.
